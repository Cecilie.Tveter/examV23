package INF101.exam;

/**
 * This data structure computes the average of the last elements added.
 * 
 * @author Martin Vatshelle
 */
public interface IAverage {

	/**
	 * Adds an integer to this datastructure
	 * 
	 * @param measurement the integer to add
	 */
	void add(int measurement);

	
	/**
	 * Computes the average of the last elements added
	 * @return
	 */
	double average();
}
